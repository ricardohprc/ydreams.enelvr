﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezerBehavior : InteractableObject {

    public override void ToogleAll()
    {
        FreezerBehavior[] lampadas = FindObjectsOfType<FreezerBehavior>();
        foreach (FreezerBehavior lamp in lampadas)
        {
            lamp.ToogleObject();
        }
    }
}
