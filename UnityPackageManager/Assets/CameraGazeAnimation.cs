﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraGazeAnimation : MonoBehaviour {

    public GameObject GazeObject;
    public Animator BubbleAnimator;
    public Text NameObject, ObjectKWH;
    public Image ProgressBar;
    public Vector3 Reference, debug;
    private float TimeForAnimation = 1;
    public static CameraGazeAnimation instance;

    // Use this for initialization
    void Start () {
        instance = this;

    }

    public void SetGazePosition(RaycastHit hit)
    {
        GazeObject.transform.position = hit.point;
        Vector3 objectForward = this.transform.TransformDirection(Reference);
        GazeObject.transform.localRotation = Quaternion.LookRotation(objectForward, hit.normal);
    }

    public void UpdatedGazedObject(InteractableObject data)
    {
    }

    public void UpdatedProgressBar(float progress)
    {
        ProgressBar.fillAmount = progress;
    }

    public void GazingAtInteractable(RaycastHit hit)
    {
        
        InteractableObject data = hit.transform.GetComponent<InteractableObject>();
        if (data.Name.Length > 0)
        {
            BubbleAnimator.SetBool("Animate", true);
            NameObject.text = data.Name;
            ObjectKWH.text = data.Kwh > 0 ? data.Kwh.ToString() + " kWH" : "";
        }
    }

    public void GazingAtNonInteractable()
    {
        BubbleAnimator.SetBool("Animate", false);
    }
}
