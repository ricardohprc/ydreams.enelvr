﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneBehavior : MonoBehaviour {

    [Header("Itens Of Scene")]
    public InteractableObject[] Objects;
    [Header("Itens When Scene Start")]
    public bool[] States;

    [Header("UI Itens")]
    public Slider EnergyBar;
    public Slider SatisfactionBar;
    public Text TimeStop;
    public Image FadeInOut, FinalScore;
    public float TotalTime;
    public bool ActiveScene;
    public SceneBehavior NextScene;
    public static SceneBehavior instance;

    private bool ShouldUpdate, ShouldUpdateTime;
    private float TotalEnergy;
    private float TotalSatisfaction;
    private float MaxSatisfaction = 7;
    private float MaxEnergy;
    private float Step = 0.1f;
    private float timespan;

    public void StartScene()
    {
        instance = this;
        ResetScene();
        UpdateUI();
        ActiveScene = true;
        ShouldUpdateTime = true;
    }

    public void ChangeScene(SceneBehavior scene)
    {
        StartCoroutine(ChangeSceneAsync(scene));
    }

    private IEnumerator ChangeSceneAsync(SceneBehavior scene)
    {
        if (instance.Objects != null && instance.Objects.Length != 0)
        {
            FinalScoreConstructor.instance.Show();
            yield return new WaitForSeconds(5f);
        }
        FadeInOut.CrossFadeAlpha(1, 0.3f, true);
        yield return new WaitForSeconds(0.5f);
        instance.gameObject.SetActive(false);
        scene.gameObject.SetActive(true);
        ActiveScene = false;
        FadeInOut.CrossFadeAlpha(0, 0.3f, true);
        FinalScoreConstructor.instance.Hide();
        scene.StartScene();
    }

    public void OnEnable()
    {
        if (instance == null)
        {
            instance = this;
            ResetScene();
            UpdateUI();
            FadeInOut.CrossFadeAlpha(0, 0, true);
        }
    }

    private void Update()
    {
        if (TimeStop.gameObject.activeInHierarchy == false)
        {
            return;
        }

        if (ShouldUpdateTime)
        {

            timespan += Time.deltaTime;
            TimeStop.text = Math.Floor(TotalTime - timespan) + "";
            TimeStop.transform.parent.GetComponent<Image>().fillAmount = 1 - (timespan / TotalTime);
        }

        if(timespan > TotalTime)
        {
            timespan = 0;
            ShouldUpdateTime = false;
            ChangeScene(NextScene);
        }

        if(CheckWhyFailed().Length == 0)
        {
            ShouldUpdateTime = false;
            ChangeScene(NextScene);
        }

        if (ShouldUpdate)
        {
            UpdateBySteps();
        }
    }

    private void UpdateBySteps()
    {
        if (EnergyBar.gameObject.activeInHierarchy == false || SatisfactionBar.gameObject.activeInHierarchy == false)
        {
            return;
        }
        bool energy = false, satis = false;
        EnergyBar.value = (TotalEnergy / MaxEnergy);
        energy = true;

        SatisfactionBar.value = (TotalSatisfaction / MaxSatisfaction);
        satis = true;

        if(energy && satis)
        {
            ShouldUpdate = false;
        }
    }

    public static void UpdateUI()
    {
        if (instance.Objects == null || instance.Objects.Length == 0)
        {
            instance.SatisfactionBar.gameObject.SetActive(false);
            instance.EnergyBar.gameObject.SetActive(false);
            instance.TimeStop.gameObject.transform.parent.gameObject.SetActive(false);
            return;

        }
        else
        {
            instance.SatisfactionBar.gameObject.SetActive(true);
            instance.EnergyBar.gameObject.SetActive(true);
            instance.TimeStop.gameObject.transform.parent.gameObject.SetActive(true);
        }
        instance.MaxSatisfaction = instance.Objects.Length;
        instance.MaxEnergy = instance.CountOfMaxEnergies();
        instance.TotalEnergy = instance.CountOfEnergies();
        instance.TotalSatisfaction = instance.CountOfObjects();
        instance.ShouldUpdate = true;
    }

    public void ResetScene()
    {
        for (int i = 0; i < Objects.Length; i++)
        {
            Objects[i].SetStatus(States[i]);
        }
    }

    public int CountOfObjects()
    {
        int result = 0;
        for (int i = 0; i < Objects.Length; i++)
        {
            result += BoolToInt(Objects[i]);
        }
        return result;
    }

    public float CountOfEnergies()
    {
        float result = 0;
        for (int i = 0; i < Objects.Length; i++)
        {
            result += GetEnergyIfActive(Objects[i]);
        }
        return result;
    }

    public float CountOfMaxEnergies()
    {
        float result = 0;
        for (int i = 0; i < Objects.Length; i++)
        {
            result += GetEnergy(Objects[i]);
        }
        return result;
    }

    public float GetEnergyIfActive(InteractableObject interactableObject)
    {
        if(interactableObject != null)
        {
            return interactableObject.Active ? interactableObject.Kwh : 0;
        }
        return 0;
    }

    public float GetEnergy(InteractableObject interactableObject)
    {
        if (interactableObject != null)
        {
            return interactableObject.Kwh;
        }
        return 0;
    }

    public string CheckWhyFailed()
    {
        string reason = "";
        Dictionary<string, int> keyValuePairs = new Dictionary<string, int>();
        for (int i = 0; i < Objects.Length; i++)
        {
            if (!keyValuePairs.ContainsKey(Objects[i].TypeOfObject) && Objects[i].Active)
            {
                keyValuePairs.Add(Objects[i].TypeOfObject, 1);
            }
            else if (keyValuePairs.ContainsKey(Objects[i].TypeOfObject) && Objects[i].Active)
            {
                return "Você está gastando muita energia com " + Objects[i].TypeOfObject;
            }
            
        }

        for (int i = 0; i < Objects.Length; i++)
        {
            if (!keyValuePairs.ContainsKey(Objects[i].TypeOfObject) && !Objects[i].Active)
            {
                return "Nenhum(a) " + Objects[i].TypeOfObject + " está ligado";
            }

        }
        return reason;
    }

    public int BoolToInt(bool value)
    {
        return value ? 1 : 0;
    }
}
