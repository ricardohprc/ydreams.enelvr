﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalScoreConstructor : MonoBehaviour {

    public Text FinalKWH, FinalSatisfacao;
    public Text ConsumoIdealLabel, WhyFailed;
    public static FinalScoreConstructor instance;

    public void Start()
    {
        instance = this;
    }

    public void Show()
    {
        Mount();
        iTween.ScaleTo(this.gameObject, Vector3.one, 0.3f);
    }

    private void Mount()
    {
        FinalSatisfacao.text = Mathf.RoundToInt(SceneBehavior.instance.SatisfactionBar.value * 100) + "%";
        FinalKWH.text = Mathf.RoundToInt(SceneBehavior.instance.EnergyBar.value * 100) + " KWH";
        if(SceneBehavior.instance.CheckWhyFailed().Length == 0)
        {
            ConsumoIdealLabel.gameObject.SetActive(true);
            WhyFailed.gameObject.SetActive(false);
        }
        else
        {
            WhyFailed.text = SceneBehavior.instance.CheckWhyFailed();
            ConsumoIdealLabel.gameObject.SetActive(false);
            WhyFailed.gameObject.SetActive(true);
        }
    }

    public void Hide()
    {
        iTween.ScaleTo(this.gameObject, Vector3.zero, 0.3f);
    }
}
