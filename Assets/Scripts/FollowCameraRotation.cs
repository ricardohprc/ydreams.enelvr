﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCameraRotation : MonoBehaviour {

	// Update is called once per frame
	void Update () {
       this.transform.localRotation = Camera.main.transform.localRotation;
	}
}
