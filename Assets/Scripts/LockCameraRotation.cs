﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockCameraRotation : MonoBehaviour {

    public float CameraAngle;
    public GameObject Target, Camera;
    public Transform OldParent;
    public bool Changed = false;
    public Vector3 CameraInitial;
    public Quaternion SceneInitial;
    private const float LimitAngle = 50;

    // Use this for initialization
    void Start () {
        Camera = this.transform.GetChild(0).gameObject;
        CameraInitial = Camera.transform.position;
        SceneInitial = Target.transform.rotation;
    }
	
	// Update is called once per frame
	void Update () {
        CameraAngle = Quaternion.Angle(Camera.transform.localRotation, this.transform.localRotation);
        if(CameraAngle > LimitAngle && !Changed)
        {
            Target.transform.parent = Camera.transform;
            Changed = true;
        }
        else if (CameraAngle <= LimitAngle && Changed)
        {
            Target.transform.parent = OldParent;
            Changed = false;
        }

        if (Target.transform.parent == OldParent)
        {
            Target.transform.localPosition = Vector3.Lerp(Target.transform.localPosition, Vector3.zero, 0.5f);
            Target.transform.localRotation = Quaternion.Lerp(Target.transform.localRotation, Quaternion.identity, 0.5f);
        }
    }
}
