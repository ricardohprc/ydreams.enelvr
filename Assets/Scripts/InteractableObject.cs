﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class InteractableObject : MonoBehaviour {

    public string Name;
    public float Kwh;
    public bool Active;
    public UnityEvent OnActive, OnDisable;
    public bool Gazed;
    public string TypeOfObject;
    public static bool GazeIsActive = true;
    internal float TimeGazed = 0;
    internal static float TimeToAction = 1.5f;
    internal ConfortControl Confort;

    public abstract void ToogleAll();

    public void Update()
    {
        if (Gazed && GazeIsActive)
        {
            TimeGazed += Time.deltaTime;
            CameraGazeAnimation.instance.UpdatedProgressBar(TimeGazed / TimeToAction);
            if (TimeGazed > TimeToAction)
            {
                ToogleAll();
                TimeGazed = 0;
                CameraGazeAnimation.instance.UpdatedProgressBar(0);
            }
        }
    }

    public void SetStatus(bool status)
    {
        Active = status;
        if (Active)
        {
            OnActive.Invoke();
        }
        else
        {
            OnDisable.Invoke();
        }
    }

    public virtual void FocusOn()
    {
        Gazed = true;
    }

    public virtual void FocusOff()
    {
        Gazed = false;
        TimeGazed = 0;
        CameraGazeAnimation.instance.UpdatedProgressBar(0);
    }

    public virtual void ToogleObject()
    {
        Active = !Active;
        SceneBehavior.UpdateUI();
        CameraGazeAnimation.instance.UpdatedGazedObject(this);
        if (Active)
        {
            OnActive.Invoke();
        }
        else
        {
            OnDisable.Invoke();
        }
    }
	
}

public class ConfortControl
{
    private float PositiveValue;
    private float NegativeValue;

    public float GetSatisfactionValue()
    {
        return PositiveValue;
    }

    public float GetDispleasureValue()
    {
        return NegativeValue;
    }
}
