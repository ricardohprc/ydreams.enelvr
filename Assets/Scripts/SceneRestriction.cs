﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneRestriction : MonoBehaviour {

    public float CameraAngle, secondCameraAngle;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        CameraAngle = Camera.main.transform.rotation.eulerAngles.y - this.transform.rotation.eulerAngles.y;
        secondCameraAngle = Mathf.Abs(CameraAngle - 180);
        if (Mathf.Abs(CameraAngle - 180) < 90)
        {
            Vector3 anctualAngles = this.transform.rotation.eulerAngles;
            Quaternion tmp = Quaternion.Lerp(Camera.main.transform.rotation, this.transform.rotation, 0.1f);
            anctualAngles.y = CameraAngle - tmp.eulerAngles.y;
            this.transform.GetChild(0).localRotation = Quaternion.Euler(anctualAngles);
        }
        else
        {
            this.transform.GetChild(0).localRotation = Quaternion.identity;
        }
    }
}
