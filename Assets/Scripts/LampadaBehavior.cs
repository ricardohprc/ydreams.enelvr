﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampadaBehavior : InteractableObject
{
    public override void ToogleAll()
    {
        LampadaBehavior[] lampadas = FindObjectsOfType<LampadaBehavior>();
        foreach(LampadaBehavior lamp in lampadas)
        {
            lamp.ToogleObject();
        }
    }
}
